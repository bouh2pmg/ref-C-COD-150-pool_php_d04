<?php

class Mars
{
    static private $id = 0;
    private $_id;
    public function __construct()
    {
        $this->_id = Mars::$id++;
    }

    public function getId()
    {
        return $this->_id;
    }
}