<?php

include_once("Mars.php");

class Astronaut
{
    static private $id = 0;
    private $name;
    private $snacks;
    private $_id;
    private $dest;

    public function __construct($name)
    {
        $this->name = $name;
        $this->snacks = 0;
        $this->_id = Astronaut::$id++;
        $this->dest = null;
        echo $name . " ready for launch !\n";
    }

    public function __destruct()
    {
        if ($this->dest)
            echo $this->name . ": Mission aborted !\n";
        else
            echo $this->name . ": I may have done nothing, but I have " . $this->snacks . " Mars to eat at least !\n";
    }

    public function getId()
    {
        return $this->_id;
    }
    
    public function getDestination()
    {
        return $this->dest;
    }

    public function getSnacks()
    {
        return $this->snacks;
    }

    public function getName()
    {
        return $this->name;
    }

    public function doActions($a = null)
    {
        if (!$a)
            {
                echo $this->name . ": Nothing to do.\n";
                return ;
            }
        if ($a instanceof Planet\Mars)
            {
                echo $this->name . ": started a mission !\n";
                $this->dest = $a;
            }
        else if ($a instanceof Chocolate\Mars)
            {
                echo $this->name . " is eating mars number " . $a->getId() . ".\n";
                ++$this->snacks;
            }
    }
};