<?php

include_once("Astronaut.php");

class Team
{
    private $name;
    private $members;

    public function __construct($name)
    {
        $this->members = Array();
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAstronauts()
    {
	return $this->members;
    }

    public function add($m)
    {
        if ($m instanceof Astronaut)
            {
                if (!in_array($m, $this->members))
                    $this->members[] = $m;
            }
        else
            echo $this->name . ": Sorry, you are not part of the team.\n";
    }

    public function remove($m)
    {
        for ($i = 0; $i < count($this->members); ++$i) # Coz fuck array_search bugged
            {
                if ($m === $this->members[$i])
                    {
                        unset($this->members[$i]);
                        $this->members = array_values($this->members);
                    }
            }
    }

    public function countMembers()
    {
        return count($this->members);
    }

    public function showMembers()
    {
        if (count($this->members) > 0)
            {
                $str = $this->name . ": ";
                for ($i = 0; $i < count($this->members); ++$i)
                    {
                        if ($i > 0)
                            $str.= ", ";
                        $str .= $this->members[$i]->getName();
                        if ($this->members[$i]->getDestination())
                            $str .= " on mission";
                        else
                            $str .= " on standby";
                    }
                echo $str . ".\n";
            }
    }
}
