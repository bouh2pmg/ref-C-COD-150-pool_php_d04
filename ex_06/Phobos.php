<?php
namespace planet\moon;
include_once("Mars.php");


class Phobos
{
    private $mars;

    public function __construct($mars)
    {
        $this->mars = $mars;
    }

    public function getMars()
    {
        return $this->mars;
    }
};