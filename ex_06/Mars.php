<?php

namespace chocolate;

class Mars
{
    static $id = 0;
    private $_id;
    public function __construct()
    {
        $this->_id = Mars::$id++;
    }

    public function getId()
    {
        return $this->_id;
    }
};

namespace planet;

class Mars
{
    static $id = 0;
    private $_id;
    private $size;

    public function __construct($size = 0)   
    {
        $this->_id = Mars::$id++;
        $this->size = $size;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getSize()
    {
        return $this->size();
    }
};
