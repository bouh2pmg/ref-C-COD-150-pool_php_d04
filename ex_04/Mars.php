<?php

namespace chocolate;

class Mars
{
    static private $id = 0;
    private $_id;
    public function __construct()
    {
        $this->_id = Mars::$id++;
    }

    public function getId()
    {
        return $this->_id;
    }
}

namespace planet;

class Mars
{
    private $size;

    public function __construct($size = 0)   
    {
        $this->size = $size;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size)
    {
        $this->size = $size;
    }
}
