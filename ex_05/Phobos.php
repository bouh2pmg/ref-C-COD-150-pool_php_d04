<?php
namespace planet\moon;
include_once("Mars.php");


class Phobos
{
    private $mars;

    public function __construct($mars)
    {
        if ($mars instanceOf \planet\Mars)
            {
                $this->mars = $mars;
                echo "Phobos placed in orbit.\n";
            }
        else
            echo "No planet given.\n";
    }

    public function getMars()
    {
        return $this->mars;
    }
};