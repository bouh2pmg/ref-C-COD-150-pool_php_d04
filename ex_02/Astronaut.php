<?php

class Astronaut
{
    static private $id = 0;
    private $_id;
    private $name;
    private $snacks;
    private $dest;
    
    public function __construct($name)
    {
        $this->_id = Astronaut::$id++;
        $this->snacks = 0;
        $this->name = $name;
        $this->dest = null;
        echo $name . " ready for launch !\n";
    }

    public function getId()
    {
        return $this->_id;            
    }

    public function getDestination()
    {
        return $this->dest;           
    }
}